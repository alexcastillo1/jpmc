//
//  ScoreModel.swift
//  20200709_alexcastillo_NYCSchools
//
//  Created by Alexander Castillo on 7/9/21.
//

import Foundation

class ScoreModel: NSObject {
    
    var dbn = ""
    var school_name = ""
    var num_of_sat_test_takers = ""
    var sat_critical_reading_avg_score = ""
    var sat_math_avg_score = ""
    var sat_writing_avg_score = ""
}
