//
//  ViewController.swift
//  20200709_alexcastillo_NYCSchools
//
//  Created by Alexander Castillo on 7/9/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ServiceProtocol  {
   
    
   
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searching = false
    
    var spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    let cellReuseIdentifier = "cell"
    var schoolList = SchoolList()
    var scores = ScoreModel()
    let schoolDataService = SchoolDataService()
    
    var filtered:[SchoolModel] = []
    
      
    override func viewDidLoad() {
        
        super.viewDidLoad()
        

        let myColor = UIColor(red: 0/255, green: 92/255, blue: 113/255, alpha: 1.0)
        self.navigationController?.navigationBar.barTintColor = myColor
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]

        view.addSubview(spinner)
        spinner.center = view.center
        TableView.delegate = self
        TableView.dataSource = self
        TableView.allowsSelection = true
        
        self.searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        
        title = "NYC Schools"
        
        getSchoolList()
      
   
    }
    
    func getSchoolList()  {
        
        schoolDataService.delegate = self
        
        schoolDataService.getSchoolList(userCompletionHandler: { schoolList, error in
            if (error != nil) {
                DispatchQueue.main.async {
                  let alert = UIAlertController(title: "School Data Service", message: error?.localizedDescription, preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                  self.present(alert, animated: true, completion: nil)
                }
                return
            }
            self.schoolList = schoolList!
            DispatchQueue.main.async {
                 self.TableView.reloadData()
                 self.TableView.layoutSubviews()
            }
           
          })
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if let selectedRow = TableView.indexPathForSelectedRow {
            TableView.deselectRow(at: selectedRow, animated: true)
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searching) {
            return self.filtered.count
        }
        return schoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style:UITableViewCell.CellStyle.subtitle, reuseIdentifier: cellReuseIdentifier)
        var school = SchoolModel()
        if (searching) {
            school = self.filtered[indexPath.row]
        }
        else {
            school = self.schoolList.schoolList[indexPath.row] 
        }
        
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.text = school.school_name
        
     //   cell.detailTextLabel?.text = school.dbn
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         var school:SchoolModel = SchoolModel()
        
        if (searching) {
            school = self.filtered[indexPath.row]
        }
        else {
            school = self.schoolList.schoolList[indexPath.row] 
        }
        let dbn = school.dbn
        
        schoolDataService.getSchoolScores(DBN: dbn, userCompletionHandler: { scores, error in
            if (error != nil) {
                if error is JSONParseError {
                    DispatchQueue.main.async {
                      let alert = UIAlertController(title: "School Data Service", message: "No Data Found...",  preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    DispatchQueue.main.async {
                       let alert = UIAlertController(title: "School Data Service", message: error?.localizedDescription, preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                    }
                }
                
                return
            }
            self.scores = scores!
            DispatchQueue.main.async {
                self.showScores()
               // self.TableView.reloadData()
            }
           
          })
        
    }
    
    func showScores() {
        let tracescores = self.scores
        
        print(tracescores)
        self.performSegue(withIdentifier: "scoredetail", sender: self)
      
    }
    
    func servicestarted() {
        DispatchQueue.main.async {
            self.spinner.startAnimating()
        }
       
       print("service started")
        sleep(2)
    }
    
    func servicedone() {
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
        }
        print("service finished")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            // get a reference to the second view controller
            let scoredetail = segue.destination as! ScoreViewController
            
            // set a variable in the second view controller with the data to pass
             scoredetail.scores = self.scores
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //    searching = true;
      }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //    searching = false;
      }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false;
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searching = false;
    }
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let temp  = schoolList.schoolList 
        
       // self.filtered = temp.filter { $0.school_name.range(of: searchText, options: .caseInsensitive) != nil }
        self.filtered = temp.filter { $0.school_name.hasPrefix(searchText) }
        
        if  (self.filtered.count == 0) {
              searching = false;
        }
        else {
              searching = true;
        }
        
        self.TableView.reloadData()
        self.TableView.layoutSubviews()
    }

}
