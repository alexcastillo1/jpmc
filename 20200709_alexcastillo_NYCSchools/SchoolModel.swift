//
//  SchoolModel.swift
//  20200709_alexcastillo_NYCSchools
//
//  Created by Alexander Castillo on 7/9/21.
//

import Foundation

class SchoolModel: NSObject {
    var dbn = ""
    var school_name = ""
    var boro = ""
    var overview = ""
    var school_10th_seats = ""
    var academic_opportunities1 = ""
    var academic_opportunities2 = ""
    
}
