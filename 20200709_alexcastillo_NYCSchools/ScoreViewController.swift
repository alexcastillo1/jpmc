//
//  ScoreViewController.swift
//  20200709_alexcastillo_NYCSchools
//
//  Created by Alexander Castillo on 7/9/21.
//

import UIKit

class ScoreViewController: UIViewController {
    
    @IBOutlet weak var school: UILabel!
    
    @IBOutlet weak var critical_reading: UILabel!
    
    @IBOutlet weak var Math: UILabel!
    
    
    @IBOutlet weak var writing: UILabel!
    
    @IBOutlet weak var takers: UILabel!
    
    var scores:ScoreModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SAT Scores"
        self.navigationController?.navigationBar.tintColor = .white
       // print(scores?.school_name ?? "")
        school.text = scores?.school_name ?? ""
        Math.text = scores?.sat_math_avg_score ?? ""
        critical_reading.text = scores?.sat_critical_reading_avg_score ?? ""
        writing.text = scores?.sat_writing_avg_score ?? ""
        takers.text = scores?.num_of_sat_test_takers ?? ""
        
        
      //  self.view.backgroundColor = .red

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
