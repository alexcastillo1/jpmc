//
//  SchoolDataService.swift
//  20200709_alexcastillo_NYCSchools
//
//  Created by Alexander Castillo on 7/9/21.
//

import Foundation

protocol ServiceProtocol: AnyObject {
    func servicestarted()
    func servicedone()
}
struct JSONParseError: Error {
    let description:String
    var errordescription:String
    
}
class SchoolDataService: NSObject {
    
    weak var delegate:ServiceProtocol?
    var schoolList = SchoolList()
    var scoremodel = ScoreModel()
    
    var school_endpoint:String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    var score_endpoint:String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    
    
    func getSchoolList(userCompletionHandler : @escaping (SchoolList?, Error?) ->Void) {
         
        let url = URL(string: school_endpoint)
        
        #if DEBUG
        let defaults = UserDefaults.standard
        let mocktest = defaults.bool(forKey: "mocktest")
        let mockfile = defaults.string(forKey: "mockfile")
        if mocktest {
            self.mockTestSchoolList(endpoint: mockfile!)
             userCompletionHandler(self.schoolList, nil)
            return
        }
        #endif
        
        
    
        let task = URLSession.shared.dataTask(with: url!, completionHandler: { data, response, error -> Void in
            self.delegate?.servicestarted()
            guard let data = data else {
                   self.delegate?.servicedone()
                   print("URLSession dataTask error:", error ?? "nil")
                   userCompletionHandler(nil, error)
                   return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [Dictionary<String, AnyObject>]
                self.parseSchools(jsondata:json!)
                self.delegate?.servicedone()
                userCompletionHandler(self.schoolList, nil)
              }
            catch {
                self.delegate?.servicedone()
                print(error.localizedDescription)
                userCompletionHandler(nil, error)
            }
        })
        task.resume()
        
           
    }
    
    func getSchoolScores(DBN: String, userCompletionHandler : @escaping (ScoreModel?, Error?) ->Void) {
        
        let endpoint = score_endpoint + DBN
        let url = URL(string: endpoint)
        
    
        let task = URLSession.shared.dataTask(with: url!, completionHandler: { data, response, error -> Void in
            self.delegate?.servicestarted()
            guard let data = data else {
                self.delegate?.servicedone()
                print("URLSession dataTask error:", error ?? "nil")
                userCompletionHandler(nil, error)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [Dictionary<String, AnyObject>]
                if  self.parseScores(jsondata:json!) {
                    self.delegate?.servicedone()
            //        NotificationCenter.default.post(name:notif, object: nil)
                    userCompletionHandler(self.scoremodel, nil)
                }
                else {
                    self.delegate?.servicedone()
                    print("No Data Found...")
                    userCompletionHandler(nil, JSONParseError(description: "No Data Found...", errordescription: "No Data Found..."))
                    return
                }
              }
            catch {
                print(error.localizedDescription)
                userCompletionHandler(nil, error)
            }
        })
        task.resume()
        
           
    }
    
    func parseSchools(jsondata:[[String:AnyObject]]) {
        
       schoolList = SchoolList()
        
       for anItem in jsondata {
         let school = SchoolModel()
        school.dbn = (anItem["dbn"] as AnyObject? as? String) ?? ""
        school.school_name  =  (anItem["school_name"]  as AnyObject? as? String) ?? ""
        school.boro  =  (anItem["boro"]  as AnyObject? as? String) ?? ""
        school.overview  =  (anItem["overview_paragraph"]  as AnyObject? as? String) ?? ""
        school.school_10th_seats  =  (anItem["school_10th_seats"]  as AnyObject? as? String) ?? ""
        school.school_name  =  (anItem["school_name"]  as AnyObject? as? String) ?? ""
        school.academic_opportunities1  =  (anItem["academicopportunities1"]  as AnyObject? as? String) ?? ""
        school.academic_opportunities2  =  (anItem["academicopportunities2"]  as AnyObject? as? String) ?? ""
        schoolList.add(school:school)
       }
   }
    
    func parseScores(jsondata:[[String:AnyObject]]) -> Bool {
        if jsondata.count == 0 {
            return false
        }
        
         for anItem in jsondata {
           scoremodel.dbn = (anItem["dbn"] as AnyObject? as? String) ?? ""
           scoremodel.school_name = (anItem["school_name"] as AnyObject? as? String) ?? ""
           scoremodel.num_of_sat_test_takers = (anItem["num_of_sat_test_takers"] as AnyObject? as? String) ?? ""
           scoremodel.sat_critical_reading_avg_score = (anItem["sat_critical_reading_avg_score"] as AnyObject? as? String) ?? ""
           scoremodel.sat_math_avg_score = (anItem["sat_math_avg_score"] as AnyObject? as? String) ?? ""
           scoremodel.sat_writing_avg_score = (anItem["sat_writing_avg_score"] as AnyObject? as? String) ?? ""
            
        }
        return true
    }
    
    private func mockTestSchoolList(endpoint:String) {
        
        let file = endpoint.split(separator: ".")
        let filename:String = String(file[0])
        
        let path = String(Bundle.main.path(forResource: filename, ofType: "json")!)
         print("mocktest")
         
         do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [Dictionary<String, AnyObject>]
                self.parseSchools(jsondata: json!)
        }
        catch {
            print("mockTestList error!")
        }
        
    }
}
