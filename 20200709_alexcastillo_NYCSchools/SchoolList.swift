//
//  SchoolList.swift
//  20200709_alexcastillo_NYCSchools
//
//  Created by Alexander Castillo on 7/9/21.
//

import Foundation

class SchoolList: NSObject {
    
    var schoolList = [SchoolModel]()
    var count = 0
    
    func add(school:SchoolModel) {
        self.schoolList.append(school)
        self.count+=1
    }
    
}
