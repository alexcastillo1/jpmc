//
//  _0200709_alexcastillo_NYCSchoolsTests.swift
//  20200709_alexcastillo_NYCSchoolsTests
//
//  Created by Alexander Castillo on 7/9/21.
//

import XCTest
@testable import _0200709_alexcastillo_NYCSchools

class _0200709_alexcastillo_NYCSchoolsTests: XCTestCase {
    
    private var schoolList : SchoolList!
    private var schooldatasvc : SchoolDataService!
    

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

   
    // exercise rest api call routine
    func testEndpoint() throws {
        
        let expected = expectation(description: "callback testing")
        
        let schooldatasvc = SchoolDataService()
        schooldatasvc.getSchoolList(userCompletionHandler: { schoolList, error in
            expected.fulfill()
            self.schoolList = schoolList!
         })
        
        wait(for: [expected], timeout: 10)
        XCTAssertGreaterThan(self.schoolList.count, 0)
        print("==============")
        print(self.schoolList.count)
        print("==============")
        
    }
    
    func testBadEndpoint() throws {
        
        let expected = expectation(description: "callback testing")
        let schooldatasvc = SchoolDataService()
        schooldatasvc.school_endpoint = "https://"
        schooldatasvc.getSchoolList(userCompletionHandler: { schoolList, error in
            expected.fulfill()
            XCTAssertNotNil(error, "error encountered")
            print("==============")
            print(error ?? "?")
            print("==============")
        })
        
        wait(for: [expected], timeout: 10)
      
    }
    
    
    // exercise service mocktest with json parser
    func testOneItemList() throws {
    
        let defaults =  UserDefaults.standard
        defaults.set(true, forKey:"mocktest")
        defaults.set("oneitem.json", forKey: "mockfile")
        
        let expected = expectation(description: "callback testing")
        let schooldatasvc = SchoolDataService()
        schooldatasvc.getSchoolList(userCompletionHandler: { schoolList, error in
            expected.fulfill()
            self.schoolList = schoolList!
    
        })
        
        wait(for: [expected], timeout: 10)
        XCTAssertEqual(self.schoolList.count, 1)
        print("==============")
        print(self.schoolList.count)
        print("==============")
    }
    

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
